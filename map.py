#!/usr/bin/python
import sys, os
abspath = os.path.dirname(__file__)
sys.path.append(abspath)
os.chdir(abspath)

import web
from osgeo import ogr, osr

urls = (
    '/', 'index',
    )

app = web.application(urls, globals())

class index:
    def GET(self):
        i = web.input(lat="", lng="", wkt="")
        driver = ogr.GetDriverByName('ESRI Shapefile')
        dataset = driver.Open('shapes/35SPAULO.shp')
        layer = dataset.GetLayerByIndex(0)
        layer.ResetReading()

        if i.wkt:
            point = ogr.CreateGeometryFromWkt(i.wkt)
        elif i.lat and i.lon:
            point = ogr.CreateGeometryFromWkt("POINT(" + str(i.lat) + " " + str(i.lng) + ")"

        if i.wkt=="*":
            lista = []
            for feature in layer:
                    lista.append(feature.GetField('NOMEDIST'))
            return lista

        if point:
            for feature in layer:
                if feature.GetGeometryRef().Contains(point):
                    return feature.GetField('NOMEDIST')
            return "None"


if __name__ == "__main__": app.run()
application = app.wsgifunc()

